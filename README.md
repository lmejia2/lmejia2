## Hi, my name is Luis Mejia, Backend Engineer based on Nicaragua and working full time remote for GitLab.


I love python, I like ruby and I'm in a love-hate relationship with javascript.

Next on my list is Go Lang.

I'm a passionate advocate for remote teams, agile, debugging and helping others by bringing a simple solutions to complex problems.
 
- 🌱 I’m currently learning *Go*
- 🤔 I’m looking for: both a mentor and a mentee
- 💬 What could I talk for 30 minutes about with absolutely no preparation?: parenting, professional/personal growth, python 
- ⚡ Fun fact: I'm a terrible singer but love singing.

 
